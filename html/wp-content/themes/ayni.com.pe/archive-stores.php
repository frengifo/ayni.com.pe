<?php get_header(); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="wrapper">
				<div class="info stores">

					<div class="row">
						
						<div class="col-md-6 col-sm-6 col-xs-12">
							
							<?php 
								$args = array(
									'post_type' => 'stores',
									'posts_per_page' => -1,
									'tax_query' => array(
										array(
											'taxonomy' => 'pais',
											'field'    => 'slug',
											'terms'    => 'united-satates',
										),
									),
								);
								$pais = new WP_Query( $args );
						 	?>

						 	<h3 class="title">UNITED STATES</h3>

							<?php while ( $pais->have_posts() ) : $pais->the_post(); ?>
				
				
								<article>
									<h4><?php the_title(); ?></h4>
									<div class="detail">
										<?php the_content(); ?>
									</div>
								</article>
								
								

							<?php endwhile; ?>

						</div>


						<div class="col-md-6 col-sm-6 col-xs-12">
							
							<?php 
								$args = array(
									'post_type' => 'stores',
									'tax_query' => array(
										array(
											'taxonomy' => 'pais',
											'field'    => 'slug',
											'terms'    => 'guatemala',
										),
									),
								);
								$pais = new WP_Query( $args );
						 	?>

						 	<h3 class="title">GUATEMALA</h3>

							<?php while ( $pais->have_posts() ) : $pais->the_post(); ?>
				
				
								<article>
									<h4><?php the_title(); ?></h4>
									<div class="detail">
										<?php the_content(); ?>
									</div>
								</article>
								
								

							<?php endwhile; ?>


							<?php 
								$args = array(
									'post_type' => 'stores',
									'tax_query' => array(
										array(
											'taxonomy' => 'pais',
											'field'    => 'slug',
											'terms'    => 'spain',
										),
									),
								);
								$pais = new WP_Query( $args );
						 	?>

						 	<h3 class="title">SPAIN</h3>

							<?php while ( $pais->have_posts() ) : $pais->the_post(); ?>
				
				
								<article>
									<h4><?php the_title(); ?></h4>
									<div class="detail">
										<?php the_content(); ?>
									</div>
								</article>
								
								

							<?php endwhile; ?>


							<?php 
								$args = array(
									'post_type' => 'stores',
									'tax_query' => array(
										array(
											'taxonomy' => 'pais',
											'field'    => 'slug',
											'terms'    => 'belgium',
										),
									),
								);
								$pais = new WP_Query( $args );
						 	?>

						 	<h3 class="title">BELGIUM</h3>

							<?php while ( $pais->have_posts() ) : $pais->the_post(); ?>
				
				
								<article>
									<h4><?php the_title(); ?></h4>
									<div class="detail">
										<?php the_content(); ?>
									</div>
								</article>
								
								

							<?php endwhile; ?>


							<?php 
								$args = array(
									'post_type' => 'stores',
									'tax_query' => array(
										array(
											'taxonomy' => 'pais',
											'field'    => 'slug',
											'terms'    => 'korea',
										),
									),
								);
								$pais = new WP_Query( $args );
						 	?>

						 	<h3 class="title">KOREA</h3>

							<?php while ( $pais->have_posts() ) : $pais->the_post(); ?>
				
				
								<article>
									<h4><?php the_title(); ?></h4>
									<div class="detail">
										<?php the_content(); ?>
									</div>
								</article>
								
								

							<?php endwhile; ?>

							<?php 
								$args = array(
									'post_type' => 'stores',
									'tax_query' => array(
										array(
											'taxonomy' => 'pais',
											'field'    => 'slug',
											'terms'    => 'japan',
										),
									),
								);
								$pais = new WP_Query( $args );
						 	?>

						 	<h3 class="title">JAPAN</h3>

							<?php while ( $pais->have_posts() ) : $pais->the_post(); ?>
				
				
								<article>
									<h4><?php the_title(); ?></h4>
									<div class="detail">
										<?php the_content(); ?>
									</div>
								</article>
								
								

							<?php endwhile; ?>

						</div>

					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>