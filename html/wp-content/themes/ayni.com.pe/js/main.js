 
jQuery(document).ready(function( $ ){ 

  //ul.menu li.menu-item-has-children:hover > ul.sub-menu
  
  $( "ul.menu li.menu-item-has-children" ).mouseenter(function() {
      $(this).children('ul.sub-menu').stop(true,true).delay(200).slideDown(400);
  })
  .mouseleave(function() {
      $(this).children('ul.sub-menu').stop(true,true).delay(3000).slideUp(1500);
  });


  //Check to see if the window is top if not then display button
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
      $('.scrollToTop').fadeIn();
    } else {
      $('.scrollToTop').fadeOut();
    }
  });
  
  //Click event to scroll to top
  $('.scrollToTop').click(function(){
    $('html, body').animate({scrollTop : 0},800);
    return false;
  });

  $(".fancybox").fancybox({
    
	'fitToView'         : false,
    'autoSize'          : false,
    maxWidth    : 410,
    maxHeight   : 610,
    'width':400, 
	'height':600,
	closeClick	: false,
	openEffect	: 'none',
	closeEffect	: 'none',
    afterLoad: function() {
          
          this.title = $(".box-fancy").eq( this.index ).find("article").html();
        
      },
      afterShow: function() {
        this.title = $(".box-fancy").eq( this.index ).find("article").html();
        
       
      },
    helpers     : { 
          overlay : {closeClick: false}, // prevents closing when clicking OUTSIDE fancybox
          title: {
              type: 'inside'
          }
      }
    });

  $(".menu-down").on("click", function(){

    $(".menu").slideToggle();

  })

  $('.slider').slick({
    autoplay:true,
    arrows:false,
    dots: false,
    slidesToShow: 3,
    slidesToScroll: 3,
    infinite: true,
    responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
  });

  $('.slider-our').slick({
    autoplay:true,
    arrows:true,
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1,
  });

})

var title ="";
var subtitle ="";
var title2 ="";
var subtitle2 ="";

function cambiar(t,s,t2,s2){
  title =t;
  subtitle =s;
  title2 =t2;
  subtitle2 =s2;
}
