<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="wrapper">
						<div class="info contact">
							
							<div class="row">
								<div class="col-md-6">
									<?php the_content(); ?>
								</div>
								<div class="col-md-6 form">
									<p>Send us a message</p>
									<hr>
									<?php echo do_shortcode( '[contact-form-7 id="459" title="Contact form 1"]' ); ?>
								</div>
							</div>
							 
						</div>
					</div>
				</div>
			</div>
		</div>

	<?php endwhile; ?>

<?php get_footer(); ?>