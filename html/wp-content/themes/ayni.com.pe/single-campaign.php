<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="wrapper">
						<div class="info">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

	<?php endwhile; ?>

<?php get_footer(); ?>