<?php 
	
	add_theme_support( 'post-thumbnails', array( 'our-universe' ) );

	function register_my_menu() {
	  register_nav_menu('header-menu',__( 'Header Menu' ));
	}
	add_action( 'init', 'register_my_menu' );
/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Fifteen 1.0
 */
function pertel_scripts() {
	
	
    // Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'fancybox-style', get_template_directory_uri() . '/js/vendor/fancybox/source/jquery.fancybox.css', array(), '2.1.5' );
	wp_enqueue_script( 'fancybox-script', get_template_directory_uri() . '/js/vendor/fancybox/source/jquery.fancybox.pack.js', array(), '2.1.5');
	// Get the post type from the query
	if ( is_singular( 'collaborations' ) || is_singular( 'our-universe' ) ) {
		wp_enqueue_style( 'slick-style', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css', array(), '1.0.6' );
		wp_enqueue_script( 'slick-script', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', array(), '1.0.6');
	}
}
add_action( 'wp_enqueue_scripts', 'pertel_scripts' );

/**
 * Add Photographer Name and URL fields to media uploader
 */
 
function be_attachment_field_credit( $form_fields, $post ) {
	$form_fields['be-photographer-name'] = array(
		'label' => 'Titulo',
		'input' => 'text',
		'value' => get_post_meta( $post->ID, 'be_photographer_name', true ),
		'helps' => '',
	);

	$form_fields['be-photographer-url'] = array(
		'label' => 'Texto',
		'input' => 'text',
		'value' => get_post_meta( $post->ID, 'be_photographer_url', true ),
		'helps' => '',
	);


	$form_fields['be-photographer-titulo'] = array(
		'label' => 'Titulo',
		'input' => 'text',
		'value' => get_post_meta( $post->ID, 'be_photographer_titulo', true ),
		'helps' => '',
	);

	$form_fields['be-photographer-texto'] = array(
		'label' => 'Texto',
		'input' => 'text',
		'value' => get_post_meta( $post->ID, 'be_photographer_texto', true ),
		'helps' => '',
	);

	return $form_fields;
}

add_filter( 'attachment_fields_to_edit', 'be_attachment_field_credit', 10, 2 );

function pure_url($url) {
   $url = preg_replace('/https?:\/\/|www./', '', $url);
   if ( strpos($url, '/') !== false ) {
      $ex = explode('/', $url);
      $url = $ex['0'];
   }
   return $url;
}

/**
 * Save values of Photographer Name and URL in media uploader
 *
 */

function be_attachment_field_credit_save( $post, $attachment ) {
	if( isset( $attachment['be-photographer-name'] ) )
		update_post_meta( $post['ID'], 'be_photographer_name', $attachment['be-photographer-name'] );

	if( isset( $attachment['be-photographer-url'] ) )
		update_post_meta( $post['ID'], 'be_photographer_url', ( $attachment['be-photographer-url'] ) );

	if( isset( $attachment['be-photographer-titulo'] ) )
		update_post_meta( $post['ID'], 'be_photographer_titulo', ( $attachment['be-photographer-titulo'] ) );

	if( isset( $attachment['be-photographer-texto'] ) )
		update_post_meta( $post['ID'], 'be_photographer_texto', ( $attachment['be-photographer-texto'] ) );

	return $post;
}

add_filter( 'attachment_fields_to_save', 'be_attachment_field_credit_save', 10, 2 );


function wpb_imagelink_setup() {
	$image_set = get_option( 'image_default_link_type' );
	
	if ($image_set !== 'none') {
		update_option('image_default_link_type', 'none');
	}
}
add_action('admin_init', 'wpb_imagelink_setup', 10);

 ?>