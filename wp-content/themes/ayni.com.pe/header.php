<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" type="image/x-icon" href="http://wplind.com/wp-content/uploads/2016/02/favicon.png">
    <link rel="apple-touch-icon" href="http://wplind.com/wp-content/uploads/2016/02/favicon.png"/>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <script src="<?php echo get_template_directory_uri() ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	<?php wp_head(); ?>
    <style type="text/css">

        html{
            margin-top: 0 !important;
        }
        #wpadminbar{
            display: none;
        }
       .info{ text-align:justify;}
    </style>
    <style>
    .fancybox-image{
        height: 100%;
        max-width: initial;
        max-height: initial;
        width: 100%;
    }
    .fancybox-wrap{
        width: 400px !important;
        height: 600px !important;
        top: 50% !important;
        margin-top: -300px;
    }
    .fancybox-outer, .fancybox-inner, .fancybox-skin{
        height:100% !important;
        padding:0 !important;
        width: 100% !important;
    }
    
    @media (max-width: 680px){
        .fancybox-overlay .fancybox-title {
            font-size: .75em;
            bottom: -4em;
            right: -10em;
        }    
        .fancybox-wrap{
            width: 300px !important;
            height: 450px !important;
            top: 50% !important;
            margin-top: -225px;
        }
        .fancybox-lock .fancybox-overlay {
            overflow: hidden;
            overflow-y: scroll;
        }
    }

    </style>
    <script>
        jQuery.noConflict();
    // Code that uses other library's $ can follow here.
    </script>
</head>

<body <?php body_class(); ?>>
	
    <header>
        <section class="box-navigation bg-color-white position-fixed">
            <a href="javascript:;" class="menu-down"><img src="<?php echo get_template_directory_uri() ?>/img/burger.jpg" alt="Menu"></a>
            <a href="<?php echo site_url(); ?>" class="logo">
                <img src="<?php echo get_template_directory_uri() ?>/img/logo.png" alt="Logo">
            </a>
            <?php 
                wp_nav_menu( array(
                    'menu' => 'menu-principal'
                ) );
            ?>
        </section>
    </header>
    <a href="#" class="scrollToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
    <section class="content-page">
        <!--Pagina Front Page-->
        <?php if (is_front_page()) { ?>
            
            <ul class="cb-slideshow">
                <li><span></span><div></div></li>
                <li><span></span><div></div></li>
                <li><span></span><div></div></li>
                <li><span></span><div></div></li>
                <li><span></span><div></div></li>
                <li><span></span><div></div></li>
            </ul>
            <h2 class="notify">
                SPRING SUMMER 17
            </h2>
        <?php } ?>
        