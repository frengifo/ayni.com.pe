<?php get_header(); ?>
    
	<?php while ( have_posts() ) : the_post(); ?>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="wrapper">
						<div class="info">
							<?php 
								$slider_images = $dynamic_featured_image->get_featured_images();
								if ($slider_images) { ?>
									<div class="slider">
										<?php foreach ($slider_images as $key => $value): ?>
											<div class="box-image">
												<img src="<?php echo $value['full'] ?>" alt="<?php the_title(); ?>">
											</div>
										<?php endforeach ?>
									</div>
								<?php } ?>
								<h2 class="title"><?php the_title(); ?></h2>
								<?php if ( $gallery = get_post_gallery( get_the_ID(), false ) ) {
							        // Loop through all the image and output them one by one.
							        $ids = explode(",", $gallery['ids'] );
							        //$ids = array_reverse($ids);

							        
							        $images = ( get_posts( array( 'post_type' => 'attachment', 'post__in' => ($ids), 'posts_per_page' => - 1, 'orderby' => 'post__in'  ) ) );
							        $i=1;
							        foreach ( $images as $src ) {  ?>                

							            <div class="col-md-4 col-sm-4 col-xs-6 box-fancy" id="<?php echo $src->ID; ?>">
							            	
							            	<a href="<?php echo $src->guid; ?>" class="fancybox" rel="gallery1">
								            	<img src="<?php echo $src->guid; ?>" class="fancy-img" width="400" height="600" alt="<?php echo $src->post_title; ?>" />
								            </a>
								            <article class="hidden">
								            	<?php if ( $src->post_excerpt != "" ): ?>
								            	
								            
									            	<h4><?php echo $src->post_title ?></h4>
									            	<p><?php echo $src->post_excerpt ?></p>
									            	<h4><?php echo $src->post_content ?></h4>
									            	<p><?php echo get_post_meta($src->ID, '_wp_attachment_image_alt', true); ?></p>

									            	<h4><?php echo get_post_meta($src->ID, 'be_photographer_url', true); ?></h4>
													<p><?php echo get_post_meta($src->ID, 'be_photographer_name', true); ?></p>
													<h4><?php echo get_post_meta($src->ID, 'be_photographer_titulo', true); ?></h4>
													<p><?php echo get_post_meta($src->ID, 'be_photographer_texto', true); ?></p>

												<?php endif ?>
								            </article>
							            </div>

							            <?php if ($i % 3 == 0): ?>
							            	<div class="clear hidden-mobile"></div>
							            <?php endif ?>
							            <?php if ($i % 2 == 0): ?>
							            	<div class="clear hidden-desktop"></div>
							            <?php endif ?>
							            <?php $i++; ?>
							    <?php  }  
								}else{ ?> 

								<?php the_content(); ?>

								<?php } ?>
							 
						</div>
					</div>
				</div>
			</div>
		</div>

	<?php endwhile; ?>

<?php get_footer(); ?>