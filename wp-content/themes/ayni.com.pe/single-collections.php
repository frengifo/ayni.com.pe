<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="wrapper">
						<div class="info">
							<?php 
								if ( $gallery = get_post_gallery( get_the_ID(), false ) ) {
							        // Loop through all the image and output them one by one.
							        $ids = explode(",", $gallery['ids'] );
							        //$ids = array_reverse($ids);

							        
							        $images = ( get_posts( array( 'post_type' => 'attachment', 'post__in' => ($ids), 'posts_per_page' => - 1, 'orderby' => 'post__in'  ) ) );
							        
							        foreach ( $images as $src ) {  ?>                

							            <div class="col-md-4 col-sm-4 col-xs-6 box-fancy" id="<?php echo $src->ID; ?>">
							            	
							            	<a href="<?php echo $src->guid; ?>" class="fancybox" rel="gallery1">
								            	<img src="<?php echo $src->guid; ?>" class="fancy-img" alt="<?php echo $src->post_title; ?>" />
								            </a>
								            <article class="hidden">
								            	<?php if ( $src->post_excerpt != "" ): ?>
								            	
								            
									            	<h4><?php echo $src->post_title ?></h4>
									            	<p><?php echo $src->post_excerpt ?></p>
									            	<h4><?php echo get_post_meta($src->ID, '_wp_attachment_image_alt', true); ?></h4>
									            	<p><?php echo $src->post_content ?></p>
									            	<h4><?php echo get_post_meta($src->ID, 'be_photographer_name', true); ?></h4>
													<p><?php echo get_post_meta($src->ID, 'be_photographer_url', true); ?></p>
													<h4><?php echo get_post_meta($src->ID, 'be_photographer_titulo', true); ?></h4>
													<p><?php echo get_post_meta($src->ID, 'be_photographer_texto', true); ?></p>

												<?php endif ?>
								            </article>
							            </div>

							            

							    <?php  }  
								}else{ ?> 

								<?php the_content(); ?>

								<?php } ?>
							 
						</div>
					</div>
				</div>
			</div>
		</div>

	<?php endwhile; ?>

<?php get_footer(); ?>