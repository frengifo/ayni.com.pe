<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="wrapper">
						<div class="info ">
							
								<?php if ( $gallery = get_post_gallery( get_the_ID(), false ) ) { ?>
								    <?php 
								    $ids = explode(",", $gallery['ids'] );
							        //$ids = array_reverse($ids);

							        
							        $images = ( get_posts( array( 'post_type' => 'attachment', 'post__in' => ($ids), 'posts_per_page' => - 1, 'orderby' => 'post__in'  ) ) );
							        $i=1;
							        

								     ?>
								    <div class="slider-our">
										<?php foreach ( $images as $src ) {  ?>
											<div class="box-image">
											
												<img src="<?php echo $src->guid ?>" alt="<?php the_title(); ?>">
											</div>
										<?php } ?>
									</div>

								<?php }else{ ?> 

									<section class="box-info">
										<?php the_content(); ?>
									</section>

								<?php } ?>
							 
						</div>
					</div>
				</div>
			</div>
		</div>

	<?php endwhile; ?>

<?php get_footer(); ?>